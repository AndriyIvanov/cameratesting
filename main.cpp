#include <iostream>
#include <string>

#include <chrono>
#include <QElapsedTimer>
#include <QByteArray>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>


using namespace std;
using namespace cv;

#define SSTR(x) static_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()

QByteArray MatToQByteArray (Mat frame)
{
    std::vector<uchar> buff;
    std::vector<int> param(2);
    param[0] = cv::IMWRITE_JPEG_QUALITY;
    param[1] = 30;
    cv::imencode(".jpg", frame, buff, param);
    QByteArray arr = QByteArray(reinterpret_cast<const char*>(buff.data()), buff.size());
    return arr;
}


int main()
{
    VideoCapture video(0); //0,2,4


    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return EXIT_FAILURE;
    }

    Mat frame;

    QElapsedTimer timer;
    timer.start();

    while(true)
    {
        // Start timer

        auto start = chrono::steady_clock::now();
        timer.restart();

        video.read(frame);
        int width = frame.cols;
        int height = frame.rows;

//*********************************************************
        QByteArray arr = MatToQByteArray(frame);
        Mat imgbuf = cv::Mat(height, width, CV_8UC3, arr.data());
        Mat frame2 = cv::imdecode(imgbuf, CV_LOAD_IMAGE_COLOR);

//*********************************************************

        string frame_dim = "frame dimensions (WxH): " + to_string(frame.rows) + "x" + to_string(frame.cols);

        auto end = chrono::steady_clock::now();

        // Calculate Frames per second (FPS)
        double time = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        int timer_elapsed = timer.elapsed();


        // Display FPS on frame
        putText(frame, frame_dim, Point(10,25), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,255,0), 2);
        putText(frame, "Time_chrono (ms) : " + SSTR(int(time)), Point(10,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,0), 2);
        putText(frame, "Time_Qt (ms) : " + SSTR(timer_elapsed), Point(10,75), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,0), 2);

        // Display frame.
        imshow("Tracking", frame);
        imshow("From QByteArray", frame2);

        // Exit if ESC pressed.
        if(waitKey(1) == 27) break;
    }


    return 0;
}

