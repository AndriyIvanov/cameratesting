QT += core
QT -= gui

TARGET = CameraTesting
CONFIG += console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++14

INCLUDEPATH +='/usr/include/aarch64-linux-gnu/qt5/'

INCLUDEPATH += `pkg-config --cflags opencv`
LIBS += `pkg-config --libs opencv`

TEMPLATE = app

SOURCES += main.cpp

